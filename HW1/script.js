let responseToJson=null;
const xhr = new XMLHttpRequest();
xhr.open("GET", "https://swapi.co/api/films/");
xhr.send();
xhr.onload = function(){
    if(xhr.status !== 200){
        console.log(xhr.statusText);
    }
    else{
        const container = document.getElementById("film-list");
        const responseToJSON = JSON.parse(xhr.response);
        responseToJson=responseToJSON.results;
        for (let i = 0; i < responseToJSON.results.length; i++){
            let liItem = document.createElement('li');
            liItem.innerHTML =`<p><span style="font-weight: bold">- Name Film:</span> ${responseToJSON.results[i].title}</p>
             <p><span style="font-weight: bold">- Episode:</span> ${responseToJSON.results[i].episode_id}</p>
             <p><span style="font-weight: bold">- Film description:</span> ${responseToJSON.results[i].opening_crawl}</p>
             <a href="#" class="btn-show" id="one" style="text-decoration: none; font-weight: bold">Character List</a>`;
            container.prepend(liItem);
        }
        console.log(container);
    }
};
document.addEventListener('click',function (event) {
    if(event.target.classList.contains("btn-show")) {
        event.preventDefault();
        let parent = event.target.parentElement;
        let id = parent.querySelectorAll('p')[1].innerText[11];
        if(parent.querySelector("ul")) {
            parent.querySelector("ul").remove();
        }
        const ul = document.createElement("ul");
        parent.appendChild(ul);
        const film = responseToJson.find(item => item['episode_id'] == id);
        film.characters.forEach(function (man) {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", `${man}`);
            xhr.send();
            xhr.onload = function(){
                if(xhr.status !== 200){
                    console.log(xhr.statusText);
                }
                else {
                    let character = JSON.parse(xhr.response).name;
                    let characterP = document.createElement("li");
                    characterP.innerHTML = character;
                    ul.append(characterP);
                }
            }
        });
    }
});